# Kubectl - docker image

This project aims to build a docker image with kubectl installed that will be used in other projects pipelines for kubernetes deployment.

## Build a newer image

You can find latest kubectl stable version here: https://storage.googleapis.com/kubernetes-release/release/stable.txt
Create a tag with a kubectl version to build this docker image with the given kubectl version.

## Usage

```bash
docker run -it registry.gitlab.com/sylvanld/devops/images/kubectl:v1.23.4
```
