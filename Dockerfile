FROM alpine

ARG KUBECTL_VERSION

RUN apk add --no-cache gettext                                                                              \
    && wget                                                                                                 \
        -qO /usr/local/bin/kubectl                                                                          \
        https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl  \
    && chmod 500 /usr/local/bin/kubectl
